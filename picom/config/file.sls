# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as picom with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('picom-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_picom', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

picom-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

picom-config-file-picom-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/picom
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - picom-config-file-user-{{ name }}-present

picom-config-file-picom-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/picom/picom.conf
    - source: {{ files_switch([
                  name ~ '-picom.conf.tmpl',
                  'picom.conf.tmpl'],
                lookup='picom-config-file-picom-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - picom-config-file-picom-dir-{{ name }}-managed

{% endif %}
{% endfor %}
{% endif %}
