# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as picom with context %}

picom-package-install-pkg-installed:
  pkg.installed:
    - name: {{ picom.pkg.name }}
