# frozen_string_literal: true

control 'picom-package-install-pkg-installed' do
  title 'should be installed'

  describe package('picom') do
    it { should be_installed }
  end
end
