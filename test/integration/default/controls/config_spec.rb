# frozen_string_literal: true

control 'picom-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'picom-config-file-picom-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/picom') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'picom-config-file-picom-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/picom/picom.conf') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Screen tearing fix #') }
  end
end
