# frozen_string_literal: true

control 'picom-package-clean-pkg-removed' do
  title 'should not be installed'

  describe package('picom') do
    it { should_not be_installed }
  end
end
