# frozen_string_literal: true

control 'picom-config-clean-picom-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/picom/picom.conf') do
    it { should_not exist }
  end
end

control 'picom-config-clean-picom-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/picom') do
    it { should_not exist }
  end
end
